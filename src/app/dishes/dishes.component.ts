import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.scss']
})
export class DishesComponent implements OnInit {

  @Input() test: number;

  dishes: any;

  constructor() { }

  ngOnInit(): void {
    this.dishes = [
      {
        name: 'Mayo Cheese Panner Sandwich',
        price: 70.0,
        count: 0
      },
      {
        name: 'Oreo Milkshake',
        price: 70.0,
        count: 0
      },
      {
        name: 'Bolognese',
        price: 150.0,
        count: 0
      }
    ];
  }

  addCount(dish: any) {
    return dish.count++;
  }

  decCount(dish: any) {
    return dish.count--;
  }
}
