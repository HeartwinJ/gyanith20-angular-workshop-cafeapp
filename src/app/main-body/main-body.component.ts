import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-body',
  templateUrl: './main-body.component.html',
  styleUrls: ['./main-body.component.scss']
})
export class MainBodyComponent implements OnInit {

  deal = [
    {
      img: 'assets/images/paneer-sandwich.jpg',
      name: 'Mayo Cheese Panner Sandwich'
    },
    {
      img: 'assets/images/oreo-milkshake.png',
      name: 'Oreo Milkshake'
    },
    {
      img: 'assets/images/bolognese.jpg',
      name: 'Bolognese'
    }
  ];

  reviews = [
    {
      name: 'Melvyn',
      title: 'Amazing Cafe',
      content: 'This is like the best cafe iv\'e been to, this is amazing in more than one way! Overall a 5/5 for the cafe.'
    },
    {
      name: 'Mashooq',
      title: 'Oh Yeah!',
      // tslint:disable-next-line: max-line-length
      content: 'Ever since the first time I went here, iv\'e been addicted to this.It has something so special that makes you want more and more of this place.'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
