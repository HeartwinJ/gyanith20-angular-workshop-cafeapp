import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  test: number;

  constructor() {
    this.test = 0;
  }

  ngOnInit(): void {
  }

}
